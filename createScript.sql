CREATE DATABASE IF NOT EXISTS todo;

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskName` varchar(100) NOT NULL,
  `created` date NOT NULL,
  `updated` date DEFAULT NULL,
  `complete` tinyint(4) DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sso` VARCHAR(30) NOT NULL,
  `email` VARCHAR(30) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `state` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `task` ADD COLUMN `user_id` INT(11) NOT NULL;
ALTER TABLE `task` ADD CONSTRAINT FK_Task FOREIGN KEY (`user_id`) REFERENCES `user` (id);

create table `role` (
  `id`  int(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);

CREATE TABLE `user_role` (
  `id`  int(11) NOT NULL AUTO_INCREMENT,
  `user_id`  int(11) NOT NULL,
  `role_id`  int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT FK_User FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT FK_User_Role FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
);

INSERT INTO `role` (`type`)
VALUES ('ROLE_USER');

INSERT INTO `role` (`type`)
VALUES ('ROLE_ADMIN');

INSERT INTO `user` (`sso`, `email`, `password`, `state`)
VALUES ('admin', 'admin@admin.com', '$2a$10$Nt66uxMExStXXyzHa3SuBuR.iVx0RY79RR3byrFJ6XkwXcDG.AlKa', 'Active');

/* Update UserRole table for admin */
INSERT INTO `user_role` (`user_id`, `role_id`)
  SELECT user.id, role.id FROM user user, role role
  where user.sso='admin' and role.type='ROLE_ADMIN';

/* Update UserRole table for users */
INSERT INTO `user_role` (`user_id`, `role_id`)
  SELECT user.id, role.id FROM user user, role role
  where user.sso!='admin' and role.type='ROLE_USER';