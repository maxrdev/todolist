$(document).ready(function() {

    var $taskWrap = $('.task-wrap');

    var renderTodoList = function(data) {
        var source   = $("#task-list").html();
        var template = Handlebars.compile(source);
        $taskWrap.empty().append(template({tasks:data}));
    };

    var getTodoList = function() {
        $.ajax({
            url : "/todo/list",
            type : "post",
            dataType: 'json',
            success: function(data) {
                renderTodoList(data);
            }
        });
    };

    $.ajax({
        url: 'isauth',
        type: 'post',
        success: function(data) {
            if(data.isAuth) {
                getTodoList();
            }
        }
    });


    $taskWrap.on('click', '.remove-task, .complete-task', function(e) {
        e.preventDefault();
        var $self = $(this);
        $.ajax({
            url: $self.attr('href'),
            dataType: 'json',
            success: function(data) {
                renderTodoList(data);
            }
        });
    });

    $taskWrap.on('click', '.add-task', function(e) {
        e.preventDefault();
        var $self = $(this);
        var taskName = $self.closest('.row').find('.task-name').val();
        if(taskName.length) {
            $.ajax({
                url: $self.attr('href'),
                type: 'post',
                dataType: 'json',
                data: {'taskName': taskName},
                success: function(data) {
                    renderTodoList(data);
                }
            });
        }
    });

    var $alertSuccess = $('#success-alert');

    $taskWrap.on('focusout', '.task-name-update', function() {
        var $self = $(this);
        var currentTaskName =  $self.val(),
            previousTaskName =  $self.attr('data-value');

        if(currentTaskName != previousTaskName && currentTaskName.length) {
            $.ajax({
                url: '/update',
                type: 'post',
                dataType: 'json',
                data: {'taskName': currentTaskName, id: $self.attr('data-id')},
                success: function(data) {
                    renderTodoList(data);
                    $alertSuccess.fadeTo(2000, 500).slideUp(500);
                }
            });
        } else {
            $self.val(previousTaskName);
        }
    });

    var $modalWindow = $('.modal-login');

    var modalClose = function() {
        $modalWindow.modal('hide');
    };

    var formSwitcher = function(classSwitcher) {
        var $modalContent = $('.modal-dialog .modal-content');
        $modalContent.removeClass('signup login').addClass(classSwitcher);
        $modalContent.find('.alert').removeClass('active');
    };

    $modalWindow.on('click', '.switch-to-signup, .switch-to-login', function() {
        var $self = $(this);
        if($self.hasClass('switch-to-signup')) {
            formSwitcher('signup');
        } else {
            formSwitcher('login');
        }
    });

    $('.login-form').submit( function(e) {

        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");

        $.ajax({
            url: formURL,
            type: 'post',
            dataType: 'json',
            data: postData,
            success: function() {
                var $navbar = $('.navbar');
                modalClose();
                getTodoList();
                $navbar.find('.login-btn').removeClass('show').addClass('hide');
                $navbar.find('.logout-wrapp').removeClass('hide').addClass('show');
                $navbar.find('.show-username').empty().text(postData[0].value);
            }
        });
        e.preventDefault();
    });

    $('.sign-up-form').submit( function(e) {
        var $self = $(this);

        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");

        $.ajax({
            url: formURL,
            type: 'post',
            dataType: 'json',
            data: postData,
            success: function(data) {
                if(data.isExist) {
                    $self.find('.exist').addClass('active');
                } else {
                    $self.find('.exist').removeClass('active');
                    $self.find('.can-login').addClass('active');
                }
            }
        });
        e.preventDefault();
    });

    $('.logout').on('click', function(e) {
        e.preventDefault();
        var $self = $(this);
        $.ajax({
            url: $self.attr('href'),
            type: 'post',
            dataType: 'json',
            success: function() {
                var $navbar = $('.navbar');
                $navbar.find('.login-btn').removeClass('hide').addClass('show');
                $navbar.find('.logout-wrapp').removeClass('show').addClass('hide');
                $navbar.find('.show-username').empty();
                $taskWrap.empty();
            }
        });
    });
});