<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title><spring:message code="label.brand.name"/> </title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='webjars/bootstrap/3.3.5/css/bootstrap.min.css'>

    <script id="task-list" type="text/x-handlebars-template">
        <div class="alert alert-success" id="success-alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
                <span>
                    <spring:message code="message.update.success" />
                </span>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <input type="text" class="form-control task-name" maxlength="150"
                       placeholder='<spring:message code="placeholder.input.task.name"/>'>
            </div>
            <a href="/todo/create" class="add-task">
                <span class="glyphicon glyphicon-plus-sign"></span>
            </a>
        </div>

        <ul class="list-group task-list">
            {{#each tasks}}
                <li class="list-group-item clearfix {{#if complete}} complete {{/if}}">
                    <div class="col-xs-11">
                        <input class="task-name-update form-control" maxlength="150" value="{{taskName}}" data-value="{{taskName}}" data-id="{{id}}">
                    </div>
                    <div class="action-wrap">
                        <a href="/todo/delete/{{id}}" class="remove-task" title="Remove">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                        <a href="/todo/complete/{{id}}" class="complete-task" title="Complete">
                            <span class="glyphicon glyphicon-ok"></span>
                        </a>
                    </div>
                </li>
            {{/each}}
        </ul>
    </script>

    <link rel="stylesheet" href="resources/css/main.css" type="text/css" />
</head>
<body>
<sec:authorize var="loggedIn" access="isAuthenticated()"/>
    <nav role="navigation" class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><spring:message code="label.brand.name"/></a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#" data-toggle="modal" data-target="#login-signup-form" class="login-btn ${loggedIn ? 'hide' : ''}">
                        <spring:message code="button.login"/>
                    </a>
                    <div class="logout-wrapp ${loggedIn ? 'show' : ''}">
                        Hey, <span class="show-username">
                        <c:choose>
                            <c:when test="${loggedIn}">
                                <sec:authentication property="principal.username" />
                            </c:when>
                        </c:choose>
                    </span>(
                        <a href="/logout" class="logout">
                            <spring:message code="button.logout"/>
                        </a>)
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="jumbotron">
            <c:url value="/j_spring_security_check" var="loginUrl" />
            <p><spring:message code="label.brand.name"/></p>
        </div>
        <div class="task-wrap">

        </div>
    </div>


    <div class="modal fade modal-login" id="login-signup-form" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content login">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">
                        <span class="title-login">
                            <spring:message code="label.login"/>
                        </span>
                        <span class="title-signup">
                            <spring:message code="label.sign"/>
                        </span>

                    </h4>
                </div>

                <div class="modal-body">

                    <form id="login" role="form" action="/login" class="login-form" method="post">
                        <div class="form-group">
                            <label for="login-username"><spring:message code="label.username"/></label>
                            <input type="text" class="form-control" id="login-username"
                                   placeholder='<spring:message code="placeholder.input.email"/>' name="username"/>
                        </div>
                        <div class="form-group">
                            <label for="login-password"><spring:message code="label.password"/></label>
                            <input type="password" class="form-control" id="login-password"
                                   placeholder='<spring:message code="placeholder.input.password"/>' name="password"/>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"/> <spring:message code="label.remeber.me"/>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-login">
                            <spring:message code="button.login"/>
                        </button>
                        <a href="#" class="switch-to-signup">
                            <spring:message code="label.sign"/>
                        </a>
                    </form>
                    <form id="register" role="form" action="/register" class="sign-up-form" method="post">
                        <div class="alert alert-danger exist">
                            <spring:message code="user.email.exist"/>
                        </div>
                        <div class="alert alert-success can-login">
                            <spring:message code="can.login.message"/>
                        </div>
                        <div class="form-group">
                            <label for="register-username"><spring:message code="label.username"/></label>
                            <input type="text" class="form-control" id="register-username"
                                   placeholder='<spring:message code="placeholder.input.username"/>' name="username"/>
                        </div>
                        <div class="form-group">
                            <label for="register-email"><spring:message code="label.email"/></label>
                            <input type="email" class="form-control" id="register-email"
                                   placeholder='<spring:message code="placeholder.input.email"/>' name="email"/>
                        </div>
                        <div class="form-group">
                            <label for="register-password"><spring:message code="label.password"/></label>
                            <input type="password" class="form-control" id="register-password"
                                   placeholder='<spring:message code="placeholder.input.password"/>' name="password"/>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-login">
                            <spring:message code="button.signup"/>
                        </button>
                        <a href="#" class="switch-to-login">
                            <spring:message code="label.login"/>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="webjars/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="webjars/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.3/handlebars.min.js"></script>
    <script type="text/javascript" src="resources/js/main.js"></script>

</body>
</html>
