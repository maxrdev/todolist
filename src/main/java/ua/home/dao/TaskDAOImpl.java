package ua.home.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.home.model.Task;

import java.util.List;

@Repository
@Transactional
public class TaskDAOImpl implements TaskDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void createTask(Task task) {
        getCurrentSession().save(task);
    }

    public Task getTask(Integer id) {
        return (Task) getCurrentSession().get(Task.class, id);
    }

    public void updateTaskName(Task task) {
        getCurrentSession().update(task);
    }

    public List<Task> getAllByUserId(Integer id) {
        return getCurrentSession().createQuery("from Task t where t.user.id=:user").setParameter("user", id).list();
    }

    public void deleteTask(Task task) {
        getCurrentSession().delete(task);
    }

    public void completeTask(Task task) {
        getCurrentSession().update(task);
    }
}
