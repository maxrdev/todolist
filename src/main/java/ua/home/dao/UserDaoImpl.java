package ua.home.dao;

import org.springframework.stereotype.Repository;
import ua.home.model.User;

import java.util.List;

@Repository
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

    public User findById(int id) {
        return (User)getCurrentSession().createQuery("from User u where u.id=:id").setParameter("id", id).uniqueResult();
    }

    public User findBySSO(String sso) {
        User user =  (User)getCurrentSession().createQuery("from User u where u.sso=:sso").setParameter("sso", sso).uniqueResult();
        return user;
    }

    public List<User> findByEmailOrUsername(String email, String username) {
        List<User> user =  getCurrentSession().createQuery("from User u where u.email=:email or u.sso=:username")
                .setParameter("email", email)
                .setParameter("username", username).list();
        return (List<User>)user;
    }

    public void save(User user) {
        getCurrentSession().save(user);
    }
}
