package ua.home.dao;

import ua.home.model.Task;

import java.util.List;

public interface TaskDAO {

    List<Task> getAllByUserId(Integer id);
    Task getTask(Integer id);
    void createTask(Task task);
    void updateTaskName(Task task);
    void deleteTask(Task task);
    void completeTask(Task task);
}
