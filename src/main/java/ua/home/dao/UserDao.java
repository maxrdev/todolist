package ua.home.dao;

import ua.home.model.User;

import java.util.List;

public interface UserDao {

    User findById(int id);

    User findBySSO(String sso);

    List<User> findByEmailOrUsername(String email, String username);

    void save(User user);
}
