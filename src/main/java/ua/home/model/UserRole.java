package ua.home.model;

public enum UserRole {

    USER("ROLE_USER"),
    ADMIN("'ROLE_ADMIN'");

    String userRole;

    private UserRole(String userRole){
        this.userRole = userRole;
    }

    public String getUserProfileType(){
        return userRole;
    }
}
