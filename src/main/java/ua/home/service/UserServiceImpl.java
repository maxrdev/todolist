package ua.home.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.home.dao.UserDao;
import ua.home.model.State;
import ua.home.model.User;

import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    public User findById(int id) {
        return userDao.findById(id);
    }

    public User findBySso(String sso) {
        return userDao.findBySSO(sso);
    }

    public List<User> findByEmailOrUsername(String email, String username) {
        return userDao.findByEmailOrUsername(email, username);
    }

    public void save(String username, String password, String email) {
        User user = new User();
        user.setEmail(email);
        user.setSso(username);
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        user.setPassword(hashedPassword);
        user.setState(State.ACTIVE.getState());
        userDao.save(user);
    }
}
