package ua.home.service;

import ua.home.model.User;

import java.util.List;

public interface UserService {

    User findById(int id);

    User findBySso(String sso);

    List<User> findByEmailOrUsername(String email, String username);

    void save(String username, String password, String email);
}
