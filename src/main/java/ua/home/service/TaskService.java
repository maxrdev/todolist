package ua.home.service;

import ua.home.model.Task;

import java.util.List;

public interface TaskService {

    List<Task> getAll();
    void createTask(String taskName);
    void updateTaskName(Integer id, String taskName);
    Task getTask(Integer id);
    void deleteTask(Integer id);
    void completeTask(Integer id);
}
