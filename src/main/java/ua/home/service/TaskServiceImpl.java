package ua.home.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ua.home.dao.TaskDAO;
import ua.home.model.Task;
import ua.home.model.User;

import java.util.Date;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private UserService userService;

    public List<Task> getAll() {
        return taskDAO.getAllByUserId(getCurrentUser().getId());
    }

    private User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        return  userService.findBySso(name);
    }

    public void createTask(String taskName) {
        Task task = new Task();
        Date date = new Date();
        task.setTaskName(taskName);
        task.setCreated(date);
        task.setUpdated(date);
        task.setUser(getCurrentUser());
        taskDAO.createTask(task);
    }

    public void updateTaskName(Integer id, String taskName) {
        Task task = getTask(id);
        task.setTaskName(taskName);
        task.setUpdated(new Date());
        taskDAO.updateTaskName(task);
    }

    public void deleteTask(Integer id) {
        Task task = taskDAO.getTask(id);
        taskDAO.deleteTask(task);
    }

    public void completeTask(Integer id) {
        Task task = taskDAO.getTask(id);
        if(!task.getComplete()) {
            task.setComplete(true);
        } else {
            task.setComplete(false);
        }
        taskDAO.completeTask(task);
    }

    public Task getTask(Integer id) {
        return taskDAO.getTask(id);
    }
}
