package ua.home.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.home.model.Task;
import ua.home.service.TaskService;

import java.util.List;

@Controller
@Secured("ROLE_USER")
public class TodoController {

    @Autowired
    private TaskService taskService;

    @RequestMapping("/todo/list")
    public @ResponseBody List<Task> getAll() {
        return taskService.getAll();
    }

    @RequestMapping(value = "/todo/create")
    public @ResponseBody List<Task> create(@RequestParam("taskName") String taskName) {
        if (taskName.length() != 0) {
            taskService.createTask(taskName);
        }
        return taskService.getAll();
    }

    @RequestMapping("/todo/update")
    public @ResponseBody List<Task> update(@RequestParam("taskName") String taskName, @RequestParam("id") int id) {
        taskService.updateTaskName(id, taskName);
        return taskService.getAll();
    }

    @RequestMapping("/todo/delete/{id}")
    public @ResponseBody List<Task> delete(@PathVariable int id) {
        taskService.deleteTask(id);
        return taskService.getAll();
    }

    @RequestMapping("/todo/complete/{id}")
    public @ResponseBody List<Task> complete(@PathVariable int id) {
        taskService.completeTask(id);
        return taskService.getAll();
    }
}
