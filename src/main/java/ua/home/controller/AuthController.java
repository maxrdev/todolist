package ua.home.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.home.model.User;
import ua.home.service.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
public class AuthController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, Boolean> register(@RequestParam("username") String username, @RequestParam("email") String email, @RequestParam("password") String password) {
        List<User> userList = userService.findByEmailOrUsername(email, username);
        if(userList.size() != 0) {
            return Collections.singletonMap("isExist", true);
        } else {
            userService.save(username, password, email);
            return Collections.singletonMap("isExist", false);
        }
    }

    @RequestMapping(value = "isauth", method = RequestMethod.POST)
    public @ResponseBody  Map<String, Boolean> isAuth() {
        Authentication  auth = SecurityContextHolder.getContext().getAuthentication();
        Boolean isAuth = auth.isAuthenticated();
        if(isAuth && !(auth instanceof AnonymousAuthenticationToken)) {
            return Collections.singletonMap("isAuth", true);
        } else {
            return Collections.singletonMap("isAuth", false);
        }
    }
}
