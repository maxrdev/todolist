package ua.home.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@Secured("ROLE_ADMIN")
public class AdminContoroller {

    @RequestMapping("admin")
    public @ResponseBody
    Map<String, Boolean> admin() {
        return Collections.singletonMap("success", true);
    }
}
