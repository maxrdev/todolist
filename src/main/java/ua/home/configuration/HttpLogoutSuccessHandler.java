package ua.home.configuration;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class HttpLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException {
        if(RequestUtil.isAjaxRequest(request)) {
            RequestUtil.sendJsonResponse(response, "success", "true");
        } else {
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().flush();
        }
    }


}